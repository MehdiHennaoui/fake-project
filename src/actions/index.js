import { ADD_CARD, DELETE_CARD, SELECT_LANGUAGE, NUMBER_CARDS, UPDATE_CARD, DELETE_ALL } from "../constants/actions-types";

export function addCard(payload) {
    return { type: ADD_CARD, payload }
}

export function deleteCard(id) {
    return { type: DELETE_CARD, id}
}

export function updateCard(index, payload) {
    return { type: UPDATE_CARD, index, payload}
}


export function deleteAll() {
    return { type: DELETE_ALL }
}

export function selectLanguage(lang) {
    return { type: SELECT_LANGUAGE, ...lang }
}

export function numberOfCards(int) {
    return {type: NUMBER_CARDS, ...int}
}
