export const ADD_CARD = "ADD_CARD";
export const DELETE_CARD = "DELETE_CARD";
export const UPDATE_CARD = "UPDATE_CARD";
export const SELECT_LANGUAGE = "SELECT_LANGUAGE";
export const NUMBER_CARDS = "NUMBER_CARDS";
export const DELETE_ALL = "DELETE_ALL"
