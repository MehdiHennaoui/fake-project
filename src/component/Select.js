import React, {Component} from 'react';
import { connect } from 'react-redux';
import { selectLanguage } from "../actions";

class Select extends Component {
  onChangeLanguage = (e) => {
    const action = {type: 'SELECT_LANGUAGE', payload: e.target.value}
    this.props.selectLanguage(action)
  }

  render() {
    return(
        <label>
          Language
          <select className='smp-input' name="" id="" onChange = {this.onChangeLanguage}>
            <option name='fr' value="fr">FR</option>
            <option name='en' value="en">EN</option>
          </select>
        </label>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectLanguage:  language => dispatch(selectLanguage(language))
  }
};

const mapStateToProps = (globalState) => {
  return {
    language : globalState.language,
  }
}

const SelectLanguage = connect(mapStateToProps,mapDispatchToProps)(Select);

export default SelectLanguage
