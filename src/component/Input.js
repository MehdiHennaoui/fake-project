import React, {Component} from 'react';
import { connect } from 'react-redux';
import { numberOfCards } from "../actions";
import {InputNumber} from "antd";


class Input extends Component {
  onChange = (value) => {
    const action = {type: 'NUMBER_CARDS', payload: value};
    this.props.numberOfCards(action)
  }

  render() {
    return(
      <label htmlFor="input">
        How many cards should be generated?:
        <InputNumber className='smp-input' type="number" for='input' defaultValue="1" onChange={this.onChange}/>
      </label>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    numberOfCards:  number => dispatch(numberOfCards(number))
  }
};

const mapStateToProps = (globalState) => {
  return {
    numberOfCards : globalState.numberOfCards,
  }
}

const InputCards = connect(mapStateToProps,mapDispatchToProps)(Input);

export default InputCards;
