import React from 'react';
import {connect} from 'react-redux';
import CardFake from "./CardFake";


const mapStateToProps = state => {
    return {
        cards: state.cards,
    }
};

const ConnectedList = ({cards, dispatch}) => {
    return (
        <div className="container">
            {Array.isArray(cards) && cards.map(({id, name, avatar}) => {
                return <CardFake
                    key={id}
                    name={name}
                    avatar={avatar}
                    id={id}
                    classname="card"
                />
                }
            )}
        </div>
    )
};

const List = connect(mapStateToProps)(ConnectedList);

export default List;
