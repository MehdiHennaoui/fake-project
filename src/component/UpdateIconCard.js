import React from 'react'
import {Icon} from 'antd';
import {updateCard} from '../actions';
import {connect} from "react-redux";
import fakerFR from "faker/locale/fr";
import faker from "faker/locale/en";

const mapDispatchToProp = (dispatch) => {
    return {
        updateCard: (index,card) => dispatch(updateCard(index, card))
    }
};

const handleClick = (updateCard, id, cards, lang) => {
    const indexCard = cards.findIndex(card => id === card.id);
    const selectCard = cards[indexCard];
    if (lang === 'fr') {
        updateCard(indexCard, {
            id: selectCard.id,
            name: `${fakerFR.name.firstName()} ${fakerFR.name.lastName()}`,
            avatar: fakerFR.image.avatar(),
        });
    } else {
        updateCard(indexCard, {
            id: selectCard.id,
            name: `${faker.name.firstName()} ${faker.name.lastName()}`,
            avatar: faker.image.avatar(),
        });
    }

};

const mapStateToProps = state => {
    return {
        cards: state.cards,
        lang: state.language
    }
};

const UpdateIcon = ({updateCard, id, cards, lang}) => {
    return <Icon type="sync" onClick={() => handleClick(updateCard, id, cards, lang)}/>;
};

const IconUpdateCard = connect(mapStateToProps, mapDispatchToProp)(UpdateIcon);

export default IconUpdateCard;
