import React from 'react';
import { connect } from 'react-redux';
import uuid from 'uuid';

import { deleteAll }  from '../actions';
import {Button} from "antd";

const mapDispatchToProps = (dispatch) =>  {
  return {
    deleteAll: () => dispatch(deleteAll())
  }
};

class ButtonDelete extends React.Component {
  render() {
    return (
      <Button className='smp-button delete' onClick={() => this.props.deleteAll()}>
        Delete All
      </Button>
    )
  }
};

const ButtonDeleteAll = connect(null,mapDispatchToProps)(ButtonDelete);

export default ButtonDeleteAll;
