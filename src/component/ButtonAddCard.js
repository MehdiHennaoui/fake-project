import React from 'react';
import { connect } from 'react-redux';
import uuid from 'uuid';

import { addCard }  from '../actions';
import {Button} from "antd";
import PropTypes from "prop-types";

import fakerFR from 'faker/locale/fr';
import fakerEN from 'faker/locale/en';

const mapDispatchToProps = (dispatch) =>  {
    return {
        addCard: card => dispatch(addCard(card))
    }
};

const handleClick = (addCard, props) =>{
    for(var i = 0; i < props.numberOfCards; i ++ ){
        if(props.language === 'fr') {
                addCard({
                    id: uuid(),
                    name: `${fakerFR.name.firstName()} ${fakerFR.name.lastName()}`,
                    avatar: fakerFR.image.avatar(),
                })
        }else {
            addCard({
                id: uuid(),
                name: `${fakerEN.name.firstName()} ${fakerEN.name.lastName()}`,
                avatar: fakerEN.image.avatar(),
            })
        }
    }

};

class ButtonAdd extends React.Component {
    render() {
        const {addCard} = this.props;
        return (
        <Button className="smp-button add" onClick={() => handleClick(addCard, this.props)}>
           Generate cards
        </Button>
        )
    }
};

ButtonAdd.propTypes = {
    addCard: PropTypes.func,
}

const mapStateToProps = (globalState) => {
    return {
        language: globalState.language,
        numberOfCards: globalState.numberOfCards,
    }
}

const ButtonAddCard = connect(mapStateToProps,mapDispatchToProps)(ButtonAdd);

export default ButtonAddCard;
