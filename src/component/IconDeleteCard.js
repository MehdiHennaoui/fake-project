import React from 'react';
import { connect } from 'react-redux';
import { deleteCard } from "../actions";
import { Icon } from "antd";

const mapDispatchToProps = (dispatch) => {
    return {
        deleteCard:  id => dispatch(deleteCard(id))
    }
};

const IconDelete = ({id,deleteCard}) => {
    return (
        <Icon type="delete" them="twoTime" twoToneColor="#f5222d" onClick={() => deleteCard(id)}>
            Delete Card
        </Icon>
    )
};

const IconDeleteCard = connect(null, mapDispatchToProps)(IconDelete);

export default IconDeleteCard;

