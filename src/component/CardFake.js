import { Card } from 'antd';
import React from 'react';
import IconDeleteCard from './IconDeleteCard';
import IconUpdateCard from './UpdateIconCard';

const CardFake = ({avatar, classname, name, id}) => {
    const { Meta } = Card;
    return (
        <Card
            className={classname}
            cover={<img src={avatar} alt="avatar"/>}
            actions={[<IconDeleteCard id={id}/>,<IconUpdateCard id={id} />]}
        >
            <Meta title={name}/>
        </Card>
    )
};

export default CardFake;
