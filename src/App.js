import React from 'react';
import './App.scss';
import List from './component/List';
import ButtonAddCard from './component/ButtonAddCard';
import ButtonDeleteAll from './component/ButtonDeleteAll'
import Select from './component/Select';
import InputCards from "./component/Input";

function App() {
  return (
    <div className="App">
      <div className="header">
        <h1>Personas Generator</h1>
        <h2 className="no-span slogan">generate fake data for your projects</h2>

        <div className='inputs'>
          <Select />
          <InputCards />
        </div>
        <div className="buttons">
          <ButtonAddCard />
          <ButtonDeleteAll />
        </div>
      </div>
      <div className="content">
        <List />
      </div>

    </div>
  );
}

export default App;
