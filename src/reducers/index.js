import {ADD_CARD, DELETE_CARD, UPDATE_CARD, SELECT_LANGUAGE, NUMBER_CARDS, DELETE_ALL } from "../constants/actions-types";

const initialState = {
    cards: [],
    language: 'fr',
    numberOfCards: 1,
};

function rootReducer(state = initialState, action) {
    let newState;

    if (action.type === ADD_CARD) {
        return {cards: [...state.cards, action.payload]}
    }
    if (action.type === DELETE_CARD) {
        return {cards: [...state.cards.filter(card => card.id !== action.id)]}
    }
    if (action.type === SELECT_LANGUAGE) {
        return {
            ...state,
            language: action.payload
        }
    }
    if (action.type === NUMBER_CARDS) {
        return  {
            ...state,
            numberOfCards: action.payload
        };

    }
    if (action.type === UPDATE_CARD) {
        return {...state,
            cards: [
                ...state.cards.slice(0, action.index),
                action.payload,
                ...state.cards.slice(action.index + 1)
            ]
        }
    } if (action.type === DELETE_ALL) {
        return {
            ...state,
            cards: []
        }
    }
    return state;
}

export default rootReducer;
